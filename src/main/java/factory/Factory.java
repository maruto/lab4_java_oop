package factory;

import GUI.FactoryGUI;
import factory.information.FactoryInformation;
import factory.runnables.*;
import factory.stuff.*;
import threadpool.ThreadPool;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;



public class Factory {
    private final static Detector detector = new Detector();

    private final Storage<Basket> basketStorage;
    private final Storage<Engine> engineStorage;
    private final Storage<Accessory> accessoryStorage;
    private final Storage<Car> carStorage;

    private final ThreadPool basketSuppPool;
    private final ThreadPool engineSuppPool;
    private final ThreadPool accessSuppPool;
    private final ThreadPool dealerPool;
    private final ThreadPool workerPool;
    private final ThreadPool controllerPool;

    private final FactoryInformation factoryInformation;
    private final FactoryGUI factoryGUI;
    private final Thread factoryGUIThread;

    public Factory(){
        InputStream inputStream = getClass().getResourceAsStream("/factoryConfig.properties");
        Properties property = new Properties();
        try {
            property.load(inputStream);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        factoryInformation = new FactoryInformation("/factoryConfig.properties");

        basketStorage = new Storage<>(factoryInformation.getBasketStorageSize());
        engineStorage = new Storage<>(factoryInformation.getEngineStorageSize());
        accessoryStorage = new Storage<>(factoryInformation.getAccessStorageSize());
        carStorage = new Storage<>(factoryInformation.getCarStorageSize());

        basketSuppPool = new ThreadPool(factoryInformation.getNumBasketSupp());
        engineSuppPool = new ThreadPool(factoryInformation.getNumEngineSupp());
        accessSuppPool = new ThreadPool(factoryInformation.getNumAccessSupp());
        dealerPool = new ThreadPool(factoryInformation.getNumDealers());
        workerPool = new ThreadPool(factoryInformation.getNumWorkers());
        controllerPool = new ThreadPool(1);

        factoryGUI = new FactoryGUI(basketStorage, engineStorage, accessoryStorage, carStorage, factoryInformation);
        factoryGUIThread = new Thread(factoryGUI);
    }

    public void startWork() {

        basketSuppPool.execute( new BasketSupplierRunnable(new Supplier<>(basketStorage), factoryInformation));
        engineSuppPool.execute( new EngineSupplierRunnable( new Supplier<>(engineStorage), factoryInformation));
        for(int i = 0; i < factoryInformation.getNumAccessSupp(); ++i) {
            accessSuppPool.execute( new AccessorySupplierRunnable(new Supplier<>(accessoryStorage), factoryInformation));
        }
        for(int i = 0; i < factoryInformation.getNumDealers(); ++i) {
            dealerPool.execute(new Dealer(carStorage, factoryInformation));
        }
        controllerPool.execute(new Controller(carStorage, detector));
        for(int i = 0; i < factoryInformation.getNumWorkers(); ++i) {
            workerPool.execute(new Worker(basketStorage, engineStorage, accessoryStorage, carStorage, detector, factoryInformation));
        }

        factoryGUI.setVisible(true);
        factoryGUIThread.start();
    }

    public void stop() {
        basketSuppPool.shutdown();
        engineSuppPool.shutdown();
        accessSuppPool.shutdown();
        dealerPool.shutdown();
        workerPool.shutdown();
        controllerPool.shutdown();

    }

}


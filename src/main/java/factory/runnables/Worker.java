package factory.runnables;

import factory.Detector;
import factory.Storage;
import factory.information.FactoryInformation;
import factory.stuff.Accessory;
import factory.stuff.Basket;
import factory.stuff.Car;
import factory.stuff.Engine;

import java.util.concurrent.TimeUnit;

public class Worker implements Runnable {
    private final FactoryInformation factoryInformation;
    private final Detector detector;
    private final Storage<Basket> basketStorage;
    private final Storage<Engine> engineStorage;
    private final Storage<Accessory> accessoryStorage;
    private final Storage<Car> carStorage;

    public Worker(Storage<Basket> basketStorage, Storage<Engine> engineStorage, Storage<Accessory> accessoryStorage, Storage<Car> carStorage, Detector detector,  FactoryInformation factoryInformation) {
        this.basketStorage = basketStorage;
        this.engineStorage = engineStorage;
        this.accessoryStorage = accessoryStorage;
        this.carStorage = carStorage;
        this.detector = detector;
        this.factoryInformation = factoryInformation;
    }

    @Override
    public void run() {
        while(true) {

            synchronized (detector){
                try {
                    //System.out.println("Я воркер, жду сигнал от контроллера");
                    detector.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            //System.out.println("Я воркер, достаю детали");
            Basket basket = basketStorage.getStuff();
            Engine engine = engineStorage.getStuff();
            Accessory accessory = accessoryStorage.getStuff();

            try {
                TimeUnit.SECONDS.sleep(factoryInformation.getWorkerDelly().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //System.out.println("Я воркер, делаю машмину");
            carStorage.putStuff(new Car((int) (Math.random() * 1000), engine, basket, accessory));
        }
    }
}

package factory.runnables;

import factory.Supplier;
import factory.information.FactoryInformation;
import factory.stuff.Engine;

import java.util.concurrent.TimeUnit;

public class EngineSupplierRunnable implements Runnable {
    private final Supplier<Engine> supplier;
    private final FactoryInformation factoryInformation;

    public EngineSupplierRunnable(Supplier<Engine> supplier, FactoryInformation factoryInformation) {
        this.supplier = supplier;
        this.factoryInformation = factoryInformation;
    }

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(factoryInformation.getEngineSuppDelly().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


           //System.out.println("<---- Кладу двигло\n");
            supplier.putStuff(new Engine((int) (Math.random() * 1000)));
        }
    }
}

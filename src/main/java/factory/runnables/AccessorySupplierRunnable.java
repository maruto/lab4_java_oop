package factory.runnables;

import factory.Supplier;
import factory.information.FactoryInformation;
import factory.stuff.Accessory;

import java.util.concurrent.TimeUnit;

public class AccessorySupplierRunnable implements Runnable{
    private final Supplier<Accessory> supplier;
    private final FactoryInformation factoryInformation;

    public AccessorySupplierRunnable(Supplier<Accessory> supplier, FactoryInformation factoryInformation) {
        this.supplier = supplier;
        this.factoryInformation = factoryInformation;
    }

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(factoryInformation.getAccessSuppDelly().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            //System.out.println("<---- Кладу аккцесуар\n");
            supplier.putStuff(new Accessory((int) (Math.random() * 1000)));
        }
    }
}

package factory.runnables;

import factory.Detector;

import factory.Storage;
import factory.stuff.Car;



public class Controller implements Runnable {
    private final Storage<Car> storage;
    final Detector detector;

    public Controller(Storage<Car> storage, Detector detector) {
        this.storage = storage;
        this.detector = detector;
    }

    @Override
    public void run() {
        while(true) {
            if(storage.getOccupancy() < storage.getCapacity())
                synchronized (detector){
                    detector.notify();
                }
        }
    }
}

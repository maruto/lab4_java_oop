package factory.runnables;

import factory.Supplier;
import factory.information.FactoryInformation;
import factory.stuff.Basket;

import java.util.concurrent.TimeUnit;

public class BasketSupplierRunnable implements Runnable{
    private final Supplier<Basket> supplier;
    private final FactoryInformation factoryInformation;

    public BasketSupplierRunnable(Supplier<Basket> supplier, FactoryInformation factoryInformation) {
        this.supplier = supplier;
        this.factoryInformation = factoryInformation;
    }

    @Override
    public void run() {
        while (true) {
            try {
                TimeUnit.SECONDS.sleep(factoryInformation.getBasketSuppDelly().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            //System.out.println("<---- Кладу кузовов\n");
            supplier.putStuff(new Basket((int) (Math.random() * 1000)));
        }
    }
}

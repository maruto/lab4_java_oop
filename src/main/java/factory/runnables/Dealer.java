package factory.runnables;

import factory.Factory;
import factory.Storage;
import factory.information.FactoryInformation;
import factory.stuff.Car;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.util.concurrent.TimeUnit;


public class Dealer implements Runnable {
    private final FactoryInformation factoryInformation;
    private final Storage<Car> storage;

    private Logger logger = LoggerFactory.getLogger(Factory.class);

    public Dealer(Storage<Car> storage, FactoryInformation factoryInformation){
        this.factoryInformation = factoryInformation;
        this.storage = storage;
    }

    @Override
    public void run() {
        while(true) {
            try {
                TimeUnit.SECONDS.sleep(factoryInformation.getCarDealerDelly().getSeconds());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Car car = storage.getStuff();

            logger.info("Получение машины со склада: Dealer {}: Auto {} (Basket: {} Engine: {} Accessory: {}) ",
                    Thread.currentThread().getId(), car.getID(), car.getBasketID(), car.getEngineID(), car.getAccessID());
        }
    }
}

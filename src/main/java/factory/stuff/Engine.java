package factory.stuff;

public class Engine implements IStuff {
    private final int ID;

    public Engine(int id) { ID = id;   }

    @Override
    public int getID() { return ID; }
}

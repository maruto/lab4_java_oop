package factory.stuff;

public class Car implements IStuff {
    private final int ID;
    Engine engine;
    Basket basket;
    Accessory accessory;

    public Car(int id, Engine engine, Basket basket, Accessory accessory) {
        ID = id;
        this.engine = engine;
        this.basket = basket;
        this.accessory = accessory;
    }

    @Override
    public int getID() { return ID; }
    public int getEngineID() { return engine.getID(); }
    public int getBasketID() { return basket.getID(); }
    public int getAccessID() { return accessory.getID(); }

}

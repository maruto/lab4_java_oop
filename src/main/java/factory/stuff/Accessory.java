package factory.stuff;

public class Accessory implements IStuff {
    private final int ID;

    public Accessory(int id) { ID = id;   }

    @Override
    public int getID() { return ID; }
}

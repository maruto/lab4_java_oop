package factory.stuff;

public class Basket implements IStuff {
    private final int ID;

    public Basket(int id) {
        ID = id;
    }

    @Override
    public int getID() {
        return ID;
    }
}

package factory;


public class Supplier<T>{
    private final Storage<T> storage;

    public Supplier(Storage<T> storage){
        this.storage = storage;
    }

    public void putStuff(T stuff) {
        storage.putStuff(stuff);
    }

}
package factory;

import factory.information.StorageInfo;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class Storage <T> implements StorageInfo {
    private final int capacity;//вместимость склада
    private long stuffCounter; //колличество элементов на складе за всё врем
    private final Queue<T> storage;

    public Storage(int capacity) {
        this.capacity = capacity;
        storage = new LinkedBlockingQueue<>(capacity);
    }

    public synchronized void putStuff(T stuff){
        while(storage.size() >= capacity) {
            try {
                wait();
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
        }


        storage.add(stuff);
        stuffCounter++;
        this.notifyAll();
    }

    public synchronized T getStuff() {
        while(storage.size() < 1) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        T stuff = storage.remove();
        this.notifyAll();
        return stuff;
    }

    @Override
    public long getStuffCounter() { return stuffCounter; }

    @Override
    public int getOccupancy() { return storage.size(); }


    public int getCapacity() { return capacity; }
}

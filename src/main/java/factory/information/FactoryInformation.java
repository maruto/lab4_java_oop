package factory.information;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Properties;

public class FactoryInformation {
    private final int N_WORKERS;
    private final int N_DEALERS;
    private final int N_ACCESS_SUPPLIERS;
    private final int N_ENGINE_SUPPLIERS;
    private final int N_BASKET_SUPPLIERS;

    private Duration BASKET_SUPP_DELLY;
    private Duration  ENGINE_SUPP_DELLY;
    private Duration  ACCESS_SUPP_DELLY;
    private Duration  CAR_SUPP_DELLY;
    private Duration  WORKER_DELLY;


    private final int BASKET_STORAGE_SIZE;
    private final int ENGINE_STORAGE_SIZE;
    private final int ACCESS_STORAGE_SIZE;
    private final int CAR_STORAGE_SIZE;

    public FactoryInformation(String configFile) {
        InputStream inputStream = getClass().getResourceAsStream(configFile);
        Properties property = new Properties();
        try {
            property.load(inputStream);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        N_WORKERS = Integer.parseInt(property.getProperty("N_WORKERS"));
        N_DEALERS = Integer.parseInt(property.getProperty("N_DEALERS"));
        N_ACCESS_SUPPLIERS = Integer.parseInt(property.getProperty("N_ACCESS_SUPPLIERS"));
        N_ENGINE_SUPPLIERS = 1;
        N_BASKET_SUPPLIERS = 1;

        BASKET_STORAGE_SIZE =Integer.parseInt(property.getProperty("STORAGE_BASKET_SIZE"));
        ENGINE_STORAGE_SIZE = Integer.parseInt(property.getProperty("STORAGE_ENGINE_SIZE"));
        ACCESS_STORAGE_SIZE = Integer.parseInt(property.getProperty("STORAGE_ACCESS_SIZE"));
        CAR_STORAGE_SIZE = Integer.parseInt(property.getProperty("STORAGE_CAR_SIZE"));

        BASKET_SUPP_DELLY = Duration.ofSeconds(1);
        ENGINE_SUPP_DELLY = Duration.ofSeconds(1);
        ACCESS_SUPP_DELLY = Duration.ofSeconds(1);
        CAR_SUPP_DELLY = Duration.ofSeconds(1);
        WORKER_DELLY = Duration.ofSeconds(3);
    }



    public void setBasketSuppDelly(Duration delly) {
        BASKET_SUPP_DELLY = delly;
    }
    public void setEngineSuppDelly(Duration delly) {
        ENGINE_SUPP_DELLY = delly;
    }
    public void setAccessSuppDelly(Duration delly) {
        ACCESS_SUPP_DELLY = delly;
    }
    public void setCarDealerDelly(Duration delly) {
        CAR_SUPP_DELLY = delly;
    }
    public void setWorkerDelly(Duration delly) {
        WORKER_DELLY = delly;
    }

    public Duration getBasketSuppDelly() {
        return BASKET_SUPP_DELLY;
    }
    public Duration getEngineSuppDelly() {
        return ENGINE_SUPP_DELLY;
    }
    public Duration getAccessSuppDelly() {
        return ACCESS_SUPP_DELLY;
    }
    public Duration getCarDealerDelly() {
        return CAR_SUPP_DELLY;
    }
    public Duration getWorkerDelly() {
        return WORKER_DELLY;
    }



    public int getNumWorkers() {
        return N_WORKERS;
    }
    public int getNumDealers() {
        return N_DEALERS;
    }
    public int getNumAccessSupp() {
        return N_ACCESS_SUPPLIERS;
    }
    public int getNumBasketSupp() {
        return N_BASKET_SUPPLIERS;
    }
    public int getNumEngineSupp() {
        return N_ENGINE_SUPPLIERS;
    }



    public int getBasketStorageSize() {
        return BASKET_STORAGE_SIZE;
    }
    public int getEngineStorageSize() {
        return ENGINE_STORAGE_SIZE;
    }
    public int getAccessStorageSize() {
        return ACCESS_STORAGE_SIZE;
    }
    public int getCarStorageSize() {
        return CAR_STORAGE_SIZE;
    }


   /*
    public void setBasketStorageOccupancy(int occupancy) {
        BASKET_STORAGE_OCCUPANCY = occupancy;
    }
    public void setEngineStorageOccupancy(int occupancy) {
        ENGINE_STORAGE_OCCUPANCY = occupancy;
    }
    public void setAccessStorageOccupancy(int occupancy) {
        ACCESS_STORAGE_OCCUPANCY = occupancy;
    }
    public void setCarStorageOccupancy(int occupancy) {
        CAR_STORAGE_OCCUPANCY = occupancy;
    }

    public int getBasketStorageOccupancy() {
        return BASKET_STORAGE_OCCUPANCY;
    }
    public int getEngineStorageOccupancy() {
        return ENGINE_STORAGE_OCCUPANCY;
    }
    public int getAccessStorageOccupancy() {
        return ACCESS_STORAGE_OCCUPANCY;
    }
    public int getCarStorageOccupancy() {
        return CAR_STORAGE_OCCUPANCY;
    }
*/
}

package factory.information;

public interface StorageInfo {
    public long getStuffCounter();//счётчик, который считает общее колво элементов на слкаде за всё время
    public int getOccupancy ();  // загруженость
}

import factory.Factory;

public class Main {
    public static void main(String[] args) {

        Factory factory = new Factory();
        factory.startWork();
        factory.stop();
    }
}

package GUI;

import factory.information.FactoryInformation;
import factory.information.StorageInfo;

import javax.swing.*;
import java.awt.*;
import java.time.Duration;

public class FactoryGUI extends JFrame implements Runnable {

    private final StorageInfo basketStorage;
    private final StorageInfo engineStorage;
    private final StorageInfo accessoryStorage;
    private final StorageInfo carStorage;
    private final FactoryInformation factoryInformation;

    private final JLabel engineStore;
    private final JLabel basketStore;
    private final JLabel accessStore;
    private final JLabel carStore;


    public FactoryGUI(StorageInfo basketStorage, StorageInfo engineStorage, StorageInfo accessoryStorage,
                      StorageInfo carStorage, FactoryInformation factoryInformation){
        super("Factory");
        ////////////////////////////////////////////////////////
        this.basketStorage = basketStorage;
        this.engineStorage = engineStorage;
        this.accessoryStorage = accessoryStorage;
        this.carStorage = carStorage;
        this.factoryInformation = factoryInformation;
        ////////////////////////////////////////////////////////

        this.setBounds(100,100,400,600);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(10,1,2,0));

        Font font1 = new Font("Serif", Font.BOLD, 11);

        /////////////////////////////////////////////////////////

        basketStore = new JLabel( String.format("Basket Store:  %d/%d. Produced %d baskets.\n", basketStorage.getOccupancy(),factoryInformation.getBasketStorageSize(), basketStorage.getStuffCounter() ));
        basketStore.setFont(font1);
        engineStore = new JLabel( String.format("Engine Store:  %d/%d. Produced %d engines.\n", engineStorage.getOccupancy(),factoryInformation.getEngineStorageSize(), engineStorage.getStuffCounter() ));
        engineStore.setFont(font1);
        accessStore = new JLabel( String.format("Access Store:  %d/%d. Produced %d accessories.\n", accessoryStorage.getOccupancy(),factoryInformation.getAccessStorageSize(), accessoryStorage.getStuffCounter() ));
        accessStore.setFont(font1);
        carStore = new JLabel( String.format("Car Store:  %d/%d. Produced %d cars.\n", carStorage.getOccupancy(), factoryInformation.getCarStorageSize(), carStorage.getStuffCounter() ));
        carStore.setFont(font1);

        container.add(basketStore);
        container.add(engineStore);
        container.add(accessStore);
        container.add(carStore);


        ///////////////////////////////////////////////////////////////////////////////
        Font font = new Font("Serif", Font.BOLD, 11);

        JSlider basketSuppSlider = new JSlider(JSlider.HORIZONTAL, 1, 15, 1);
        basketSuppSlider.setFont(font);
        basketSuppSlider.setMajorTickSpacing(1);
        basketSuppSlider.setPaintTicks(true);
        basketSuppSlider.setPaintLabels(true);
        basketSuppSlider.setBorder(BorderFactory.createTitledBorder("BasketSupp Delly:\n"));
        basketSuppSlider.addChangeListener(evn -> {
            int value = ((JSlider)evn.getSource()).getValue();
            factoryInformation.setBasketSuppDelly(Duration.ofSeconds(value));
        });

        JSlider engineSuppSlider = new JSlider(JSlider.HORIZONTAL, 1, 15, 1);
        engineSuppSlider.setFont(font);
        engineSuppSlider.setMajorTickSpacing(1);
        engineSuppSlider.setPaintTicks(true);
        engineSuppSlider.setPaintLabels(true);
        engineSuppSlider.setBorder(BorderFactory.createTitledBorder("EngineSupp Delly:\n"));
        engineSuppSlider.addChangeListener(evn -> {
            int value = ((JSlider)evn.getSource()).getValue();
            factoryInformation.setEngineSuppDelly(Duration.ofSeconds(value));
        });

        JSlider accessSuppSlider = new JSlider(JSlider.HORIZONTAL, 1, 15, 1);
        accessSuppSlider.setFont(font);
        accessSuppSlider.setMajorTickSpacing(1);
        accessSuppSlider.setPaintTicks(true);
        accessSuppSlider.setPaintLabels(true);
        accessSuppSlider.setBorder(BorderFactory.createTitledBorder("AccessSupp Delly:\n"));
        accessSuppSlider.addChangeListener(evn -> {
            int value = ((JSlider)evn.getSource()).getValue();
            factoryInformation.setAccessSuppDelly(Duration.ofSeconds(value));
        });

        JSlider carDealerSlider = new JSlider(JSlider.HORIZONTAL, 1, 15, 2);
        carDealerSlider.setFont(font);
        carDealerSlider.setMajorTickSpacing(1);
        carDealerSlider.setPaintTicks(true);
        carDealerSlider.setPaintLabels(true);
        carDealerSlider.setBorder(BorderFactory.createTitledBorder("CarDealer Delly:\n"));
        carDealerSlider.addChangeListener(evn -> {
            int value = ((JSlider)evn.getSource()).getValue();
            factoryInformation.setCarDealerDelly(Duration.ofSeconds(value));
        });

        JSlider workerSlider = new JSlider(JSlider.HORIZONTAL, 1, 15, 1);
        workerSlider.setFont(font);
        workerSlider.setMajorTickSpacing(1);
        workerSlider.setPaintTicks(true);
        workerSlider.setPaintLabels(true);
        workerSlider.setBorder(BorderFactory.createTitledBorder("Worker Delly:\n"));
        workerSlider.addChangeListener(evn -> {
            int value = ((JSlider)evn.getSource()).getValue();
            factoryInformation.setWorkerDelly(Duration.ofSeconds(value));
        });


        container.add(basketSuppSlider);
        container.add(engineSuppSlider);
        container.add(accessSuppSlider);
        container.add(carDealerSlider);
        container.add(workerSlider);
        ////////////////////////////////////////////////////////////////////////////////
    }


    @Override
    public void run() {
        while(true) {
            SwingUtilities.invokeLater(() -> {
                basketStore.setText( String.format("Basket Store:  %d/%d. Produced %d baskets.\n", basketStorage.getOccupancy(), factoryInformation.getBasketStorageSize(), basketStorage.getStuffCounter() ));
                engineStore.setText( String.format("Engine Store:  %d/%d. Produced %d engines.\n", engineStorage.getOccupancy(),factoryInformation.getEngineStorageSize(), engineStorage.getStuffCounter() ));
                accessStore.setText( String.format("Access Store:  %d/%d. Produced %d accessories.\n", accessoryStorage.getOccupancy(),factoryInformation.getAccessStorageSize(), accessoryStorage.getStuffCounter() ));
                carStore.setText( String.format("Car Store:  %d/%d. Produced %d cars.\n", carStorage.getOccupancy(),factoryInformation.getCarStorageSize(), carStorage.getStuffCounter() ));
            });
            try {
                Thread.sleep(100);
            } catch (InterruptedException ignored) {}
        }
    }
}